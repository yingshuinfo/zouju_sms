# coding:utf-8
"""zouju_sms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a.json URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a.json URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a.json URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework import exceptions
from rest_framework.permissions import AllowAny
from rest_framework.renderers import CoreJSONRenderer
from rest_framework.response import Response
from rest_framework.schemas import SchemaGenerator
from rest_framework.views import APIView
from rest_framework_swagger.views import get_swagger_view

import script
from zjapp import views


def get_swagger_json_view(title=None, url=None, patterns=None, urlconf=None):
    """
    Returns schema view which renders Swagger/OpenAPI.
    """

    class SwaggerSchemaView(APIView):
        _ignore_model_permissions = True
        exclude_from_schema = True
        permission_classes = [AllowAny]
        renderer_classes = [
            CoreJSONRenderer,
            # renderers.OpenAPIRenderer,
            # renderers.SwaggerUIRenderer
        ]

        def get(self, request):
            generator = SchemaGenerator(
                title=title,
                url=url,
                patterns=patterns,
                urlconf=urlconf
            )
            schema = generator.get_schema(request=request)

            if not schema:
                raise exceptions.ValidationError(
                    'The schema generator did not return a.json schema Document'
                )

            return Response(schema)

    return SwaggerSchemaView.as_view()


urlpatterns = [
                  url(r'^admin/', admin.site.urls),
                  url(r'^zjapp/', include('zjapp.urls')),
                  url(r'^authtoken/', views.obtain_auth_token),
                  url(r'^swagger_docs/', get_swagger_view(title=u'进销存')),
                  url(r'^swagger_json/', get_swagger_json_view(title=u'进销存')),
                  url(r'^api_script/', script.api),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
