import json
import os
import sys

import django
from django.shortcuts import render_to_response
from rest_framework.renderers import CoreJSONRenderer
from rest_framework.schemas import SchemaGenerator

sys.path.extend(['D:\\PycharmProjects\\zouju_sms'])
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "zouju_sms.settings")
django.setup()


def api(request):
    render = CoreJSONRenderer()
    generator = SchemaGenerator()
    schema = generator.get_schema(public=True)
    routers = json.loads(render.render(schema, renderer_context={})).get("zjapp")
    for k, v in routers.items():
        print(k)

    return render_to_response("coreapi/api.js-tml")
