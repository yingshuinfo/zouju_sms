# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import coreapi
import coreschema
from django.db import models
from django.db.models import Sum, F
from rest_framework import permissions
from rest_framework import viewsets, routers, renderers, parsers, filters, status
# Create your views here.
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.decorators import action
from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from rest_framework.schemas import ManualSchema
from rest_framework.views import APIView
from rest_framework_filters.backends import DjangoFilterBackend

from zjapp.filters import OrderProjectModelFilter, SaleOrderProjectModelFilter
from zjapp.models import ProjectModel, PurchaseOrderModel, OrderProjectModel, DepartmentModel, CategoryModel, User, \
    FileModel, StockrModel, OrderProductStoreModel, ProductStoreModel, SaleOrderProjectModel, SaleOrderModel, \
    CustomerModel, ModuleModel, CategoryModuleModel, SupplierModel, FinanceModel, SaleStoreOutProjectModel
from zjapp.serializers import ProjectModelSerializer, PurchaseOrderModelSerializer, OrderProjectModelSerializer, \
    DepartmentModelSerializer, CategoryModelSerializer, UserSerializer, ProjectOrderSerializer, FileModelSerializer, \
    StockrModelSerializer, \
    OrderProductStoreModelSerializer, ProductStoreModelSerializer, OrderProductStoreModelListSerializer, \
    SaleOrderProjectModelSerializer, SaleOrderModelSerializer, CustomerModelSerializer, ModuleModelSerializer, \
    CategoryModuleModelSerializer, SupplierModelSerializer, FinanceModelSerializer, SaleStoreOutProjectModelSerializer, \
    SaleStoreOutProjectModelCreateSerializer, SaleOrderProjectModelCreateSerializer

router = routers.DefaultRouter()


# # 权限
# class PermissionViewSet(viewsets.ModelViewSet):
#     queryset = Permission.objects.all()
#     serializer_class = PermissionSerializer
#     http_method_names = ['get']
#     filter_class = PermissionFilter
#
#
# router.register('permissionmodel', PermissionViewSet, base_name='permissionmodel')

class ObtainAuthToken(APIView):
    throttle_classes = ()
    permission_classes = (permissions.AllowAny,)
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthTokenSerializer
    if coreapi is not None and coreschema is not None:
        schema = ManualSchema(
            fields=[
                coreapi.Field(
                    name="username",
                    required=True,
                    location='form',
                    schema=coreschema.String(
                        title="Username",
                        description="Valid username for authentication",
                    ),
                ),
                coreapi.Field(
                    name="password",
                    required=True,
                    location='form',
                    schema=coreschema.String(
                        title="Password",
                        description="Valid password for authentication",
                    ),
                ),
            ],
            encoding="application/json",
        )

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key, "userInfo": UserSerializer(instance=user).data})


obtain_auth_token = ObtainAuthToken.as_view()


class ModuleModelViewSet(viewsets.ModelViewSet):
    '''
    list:模块列表
    '''
    queryset = ModuleModel.objects.all()
    serializer_class = ModuleModelSerializer
    http_method_names = ['get']


router.register('modulemodel', ModuleModelViewSet, base_name='modulemodel')


# 部门
class DepartmentModelViewSet(viewsets.ModelViewSet):
    '''
    部门管理
    '''
    queryset = DepartmentModel.objects.all()
    serializer_class = DepartmentModelSerializer

    # def get_serializer_class(self):
    #     if self.action == "list":
    #         return DepartmentModelListSerializer
    #     else:
    #         return self.serializer_class


router.register('departmentmodel', DepartmentModelViewSet, base_name='departmentmodel')


# 角色模块权限
class CategoryModuleModelViewSet(viewsets.ModelViewSet):
    '''
    角色模块权限
    '''
    queryset = CategoryModuleModel.objects.all()
    serializer_class = CategoryModuleModelSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter)
    filter_fields = ('module', 'category')


router.register('categorymodulemodel', CategoryModuleModelViewSet, base_name='categorymodulemodel')


# 部门类别
class CategoryModelViewSet(viewsets.ModelViewSet):
    '''
    角色管理
    '''
    queryset = CategoryModel.objects.all()
    serializer_class = CategoryModelSerializer


router.register('categorymodel', CategoryModelViewSet, base_name='categorymodel')


# 用户管理
class UserModelViewSet(viewsets.ModelViewSet):
    '''
    用户管理
    '''
    queryset = User.objects.all()
    serializer_class = UserSerializer


router.register('usermodel', UserModelViewSet, base_name='usermodel')


# 货物
class ProjectModelViewSet(viewsets.ModelViewSet):
    '''
    货物管理
    list: 获取所有货物

    '''
    queryset = ProjectModel.objects.all()
    serializer_class = ProjectModelSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name', 'number', 'brand_name')

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return ProjectOrderSerializer
        else:
            return self.serializer_class

    @action(detail=True)
    def orders(self, request, pk):
        orders = ProjectModel.objects.get(id=pk).project_order.all()
        serializers = ProjectOrderSerializer(instance=orders, many=True)
        return Response(serializers.data)


router.register('projectmodel', ProjectModelViewSet, base_name='projectmodel')


# 订单
class PurchaseOrderModelViewSet(viewsets.ModelViewSet):
    queryset = PurchaseOrderModel.objects.all()
    serializer_class = PurchaseOrderModelSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter)
    # filter_fields = ('order',)
    search_fields = ('number',)


router.register('purchaseordermodel', PurchaseOrderModelViewSet, base_name='purchaseordermodel')


# 订单货物
class OrderProjectModelViewSet(viewsets.ModelViewSet):
    queryset = OrderProjectModel.objects.all()
    serializer_class = OrderProjectModelSerializer
    filter_class = OrderProjectModelFilter


router.register('orderprojectmodel', OrderProjectModelViewSet, base_name='orderprojectmodel')


# 文件上传
class FileModelViewSet(viewsets.ModelViewSet):
    queryset = FileModel.objects.all()
    serializer_class = FileModelSerializer
    # parser_classes = (parsers.FormParser, parsers.MultiPartParser,)


router.register('filemodel', FileModelViewSet, base_name='filemodel')


# pdf 生成器
# pdfmetrics.registerFont(TTFont('yh', 'resource/msyh.ttf'))
# DEFAULT_FONT['helvetica'] = 'yh'
#
#
# class GenPdfViewSet(viewsets.ViewSet):
#
#     def list(self, request):
#         context = {'myvar': 'this is your template context'}
#         # ProjectModel.objects.all()
#         print ProjectModel._meta.verbose_name.title()
#         template = get_template("report.html")
#         html = template.render(context)
#         # pdf = pdfkit.from_string(html, False)
#         # Create a.json Django response object, and specify content_type as pdf
#         response = HttpResponse(content_type='application/pdf')
#         response['Content-Disposition'] = 'attachment; filename="report.pdf"'
#         # find the template and render it.
#
#         # create a.json pdf
#         pisaStatus = pisa.CreatePDF(
#             html, dest=response, link_callback=link_callback)
#         # if error then show some funy view
#         if pisaStatus.err:
#             return HttpResponse('We had some errors <pre>' + html + '</pre>')
#         return response
#
#
# router.register('genpdf', GenPdfViewSet, base_name='genpdf')


class StockrModelViewSet(viewsets.ModelViewSet):
    '''
    adjust:调库 [{"from_store":1,"products":[{"id":2,"count":3}]}]
    '''
    queryset = StockrModel.objects.all()
    serializer_class = StockrModelSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter)
    # filter_fields = ('order',)
    search_fields = ('id', 'name',)

    @action(methods=['POST', ], detail=True, )
    def adjust(self, request, pk):
        to_store = StockrModel.objects.get(id=pk)
        data = request.data
        from_store = StockrModel.objects.get(id=data.get('from_store'))
        products = data.get('products')
        for product in products:
            product_id = ProjectModel.objects.get(id=product.get("id"))
            product_count = int(product.get("count"))
            fs = ProductStoreModel.objects.get(store=from_store, product=product_id)
            ts, _ = ProductStoreModel.objects.get_or_create(store=to_store, product=product_id)
            fs.change_count(-product_count)
            ts.change_count(product_count)
        return Response({"detail": u'调库成功'})


router.register('stockrmodel', StockrModelViewSet, base_name='stockrmodel')


class OrderProductStoreModelViewSet(viewsets.ModelViewSet):
    queryset = OrderProductStoreModel.objects.all()
    serializer_class = OrderProductStoreModelSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter)
    # filter_fields = ('order',)
    search_fields = ('id', 'order_project__order__number', 'order_project__project__name', 'store__name')

    def get_serializer_class(self):
        if self.action == 'list':
            return OrderProductStoreModelListSerializer
        else:
            return self.serializer_class


router.register('opstore', OrderProductStoreModelViewSet, base_name='opstore')


class ProductStoreModelViewSet(viewsets.ModelViewSet):
    '''
    仓库货物
    '''
    queryset = ProductStoreModel.objects.all()
    serializer_class = ProductStoreModelSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter)
    filter_fields = ('store', 'product')


router.register('productstore', ProductStoreModelViewSet, base_name='productstore')


# 销货单接口
class SaleOrderModelViewSet(viewsets.ModelViewSet):
    '''
    销货单接口
    '''
    queryset = SaleOrderModel.objects.all()
    serializer_class = SaleOrderModelSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter)
    filter_fields = ('id',)


router.register('saleorder', SaleOrderModelViewSet, base_name='saleorder')


# 销货单货物接口
class SaleOrderProjectModelViewSet(viewsets.ModelViewSet):
    '''
    销货单货物接口
    '''
    queryset = SaleOrderProjectModel.objects.all()
    serializer_class = SaleOrderProjectModelSerializer
    filter_class = SaleOrderProjectModelFilter

    def get_serializer_class(self):
        if self.action == 'create':
            return SaleOrderProjectModelCreateSerializer
        else:
            return self.serializer_class


router.register('saleorderproduct', SaleOrderProjectModelViewSet, base_name='saleorderproduct')


# 客户管理相关接口
class CustomerModelViewSet(viewsets.ModelViewSet):
    '''
    客户管理相关接口
    '''
    queryset = CustomerModel.objects.all()
    serializer_class = CustomerModelSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter)
    filter_fields = ('khlx',)
    search_fields = ('id', 'name', 'number', 'kname')


router.register('customermodel', CustomerModelViewSet, base_name='customermodel')


# 供货商接口
class SupplierModelViewSet(viewsets.ModelViewSet):
    '''
    供货商相关接口
    '''
    queryset = SupplierModel.objects.all()
    serializer_class = SupplierModelSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter)
    search_fields = ('id',)


router.register('suppliermodel', SupplierModelViewSet, base_name='suppliermodel')


# 销货单出货
class SaleStoreOutProjectModelViewSet(viewsets.ModelViewSet):
    '''
    销货单出货
    '''
    queryset = SaleStoreOutProjectModel.objects.all()
    serializer_class = SaleStoreOutProjectModelSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter)
    filter_fields = ['sale_project']
    search_fields = ('id',)

    def create(self, request, *args, **kwargs):
        serializer = SaleStoreOutProjectModelCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.create(serializer.validated_data)
        headers = self.get_success_headers(serializer.data)
        return Response({"detail": u"添加成功！"}, status=status.HTTP_201_CREATED, headers=headers)


router.register('salestoreoutproject', SaleStoreOutProjectModelViewSet, base_name='salestoreoutproject')


# 财务管理接口
class FinanceModelViewSet(viewsets.ModelViewSet):
    '''
    财务管理接口
    '''
    queryset = FinanceModel.objects.all()
    serializer_class = FinanceModelSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter)
    search_fields = ('id',)


router.register('financemodel', FinanceModelViewSet, base_name='financemodel')


# 进销存 附加功能
# 到货总数量  销货总数量  进货总金额 销货总金额

class ProjectStatisticsViewSet(viewsets.ViewSet):
    def list(self, request):
        in_count = OrderProductStoreModel.objects.aggregate(Sum('count')).get("count__sum", 0)
        out_count = SaleStoreOutProjectModel.objects.aggregate(Sum('count')).get("count__sum", 0)
        in_price = OrderProductStoreModel.objects.aggregate(
            total=Sum(F("order_project__purchase_price") * F("count"), output_field=models.FloatField())).get("total")
        out_price = 0
        for ssp in SaleStoreOutProjectModel.objects.all():
            out_price += ssp.count * SaleOrderProjectModel.objects.filter(order=ssp.sale_project,
                                                                          project=ssp.product_store.product).first().sale_price
        return Response({
            "in_count": in_count,
            "out_count": out_count,
            "in_price": in_price,
            "out_price": out_price,
        })


router.register('projectstatistics', ProjectStatisticsViewSet, base_name='projectstatistics')
