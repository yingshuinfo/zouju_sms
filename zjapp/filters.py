# coding=utf-8
import rest_framework_filters as filters

from zjapp.models import OrderProjectModel, SaleOrderProjectModel


# 进货单
class OrderProjectModelFilter(filters.FilterSet):
    class Meta:
        model = OrderProjectModel
        fields = ["order", ]


class SaleOrderProjectModelFilter(filters.FilterSet):
    class Meta:
        model = SaleOrderProjectModel
        fields = ["order", "project"]
