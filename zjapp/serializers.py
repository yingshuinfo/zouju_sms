# coding:utf-8
from django.contrib.auth.models import Permission
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.serializers import raise_errors_on_nested_writes
from rest_framework.utils import model_meta

from zjapp.models import ProjectModel, PurchaseOrderModel, OrderProjectModel, DepartmentModel, CategoryModel, User, \
    FileModel, StockrModel, OrderProductStoreModel, ProductStoreModel, SaleOrderModel, SaleOrderProjectModel, \
    CustomerModel, ModuleModel, CategoryModuleModel, SupplierModel, FinanceModel, SaleStoreOutProjectModel

from drf_extra_fields.fields import Base64ImageField


# 权限列表
class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = '__all__'


# 模块
class ModuleModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModuleModel
        fields = '__all__'


class DepartmentModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = DepartmentModel
        fields = '__all__'


class CategoryModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoryModel
        fields = '__all__'


class CategoryModuleModelSerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(source='category.name', read_only=True)
    module_name = serializers.CharField(source='module.name', read_only=True)

    class Meta:
        model = CategoryModuleModel
        fields = '__all__'
        extra_fields = ['category_name', 'module_name']


# class DepartmentModelListSerializer(serializers.ModelSerializer):
#     category = CategoryModelSerializer(many=True)
#
#     class Meta:
#         model = DepartmentModel
#         fields = ["id", "name", "category", "mark"]


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

    def update(self, instance, validated_data):
        user = super(UserSerializer, self).update(instance, validated_data)
        if validated_data.get('password'):
            user.set_password(validated_data['password'])
            user.save()
        return user

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class ProjectModelSerializer(serializers.ModelSerializer):
    images = serializers.CharField()

    class Meta:
        model = ProjectModel
        fields = ["id", "images", "number", "brand_name", "name", "mark", "create_time"]


class PurchaseOrderModelSerializer(serializers.ModelSerializer):
    # products =
    supplier_name = serializers.ReadOnlyField(source='supplier.name')

    class Meta:
        model = PurchaseOrderModel
        fields = '__all__'
        extra_fields = ['supplier_name']


class OrderProjectModelSerializer(serializers.ModelSerializer):
    product_name = serializers.ReadOnlyField(source='project.name')

    class Meta:
        model = OrderProjectModel
        fields = '__all__'
        extra_fields = ['product_name']


class OrderProjecOrderSerializer(serializers.ModelSerializer):
    order_number = serializers.CharField(source="order.number")
    order_create_time = serializers.CharField(source="order.create_time")

    class Meta:
        model = OrderProjectModel
        fields = ["count", "purchase_price", "suggest_price", "order_number", "order_create_time"]


class ProjectOrderSerializer(serializers.ModelSerializer):
    project_order = OrderProjecOrderSerializer(many=True)
    images = serializers.CharField(read_only=True)

    class Meta:
        model = ProjectModel
        fields = ["id", "images", "number", "brand_name", "name", "mark", "create_time", "project_order"]


class StockrModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = StockrModel
        fields = "__all__"


class OrderProductStoreModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderProductStoreModel
        fields = '__all__'


class OrderProductStoreModelListSerializer(serializers.ModelSerializer):
    order = serializers.ReadOnlyField(source='order_project.order.number')
    product = serializers.ReadOnlyField(source='order_project.project.name')
    store = serializers.ReadOnlyField(source='store.name')

    class Meta:
        model = OrderProductStoreModel
        fields = ['id', 'order', 'product', 'batch', 'count', 'store', 'state', 'create_time']


class ProductStoreModelSerializer(serializers.ModelSerializer):
    product_name = serializers.ReadOnlyField(source='product.name')
    product_images = serializers.ReadOnlyField(source='product.images')
    store_name = serializers.ReadOnlyField(source='store.name')

    class Meta:
        model = ProductStoreModel
        fields = '__all__'
        extra_fields = ['product_name', "store_name", 'product_images']


class AdjustProjectStoreSerializer(serializers.ModelSerializer):
    to_store = serializers.IntegerField()


# 销货单 序列化
class SaleOrderModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = SaleOrderModel
        fields = "__all__"


# 销货单 货物序列化
class SaleOrderProjectModelSerializer(serializers.ModelSerializer):
    product_name = serializers.ReadOnlyField(source='project.name')

    class Meta:
        model = SaleOrderProjectModel
        fields = "__all__"
        extra_fields = ['product_name', ]


class SaleOrderProjectModelCreateSerializer(serializers.ModelSerializer):
    # store_count = serializers.JSONField()
    class Meta:
        model = SaleOrderProjectModel
        fields = ["order", "project", "suggest_price", "sale_price", "count"]


# 客户管理序列化
class CustomerModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerModel
        fields = "__all__"


# 文件服务
class FileModelSerializer(serializers.ModelSerializer):
    file = Base64ImageField(use_url=False)

    class Meta:
        model = FileModel
        fields = ["file", "create_time"]


# 供货商序列化
class SupplierModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = SupplierModel
        fields = "__all__"


# 财务管理序列化
class FinanceModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = FinanceModel
        fields = "__all__"


# 销货单出货
class SaleStoreOutProjectModelSerializer(serializers.ModelSerializer):
    product_name = serializers.CharField(source="product_store.product.name", read_only=True)
    store_name = serializers.CharField(source="product_store.store.name", read_only=True)

    class Meta:
        model = SaleStoreOutProjectModel
        fields = "__all__"


# 销货单出货
class SaleStoreOutProjectModelCreateSerializer(serializers.Serializer):
    order = serializers.IntegerField()
    items = serializers.ListSerializer(child=serializers.JSONField())

    def create(self, validated_data):
        # try:
        sale_order = SaleOrderModel.objects.get(id=validated_data.get("order"))
        ssop = SaleStoreOutProjectModel.objects.filter(sale_project=sale_order).first()
        batch = 1
        if ssop:
            batch = ssop.batch + 1
        items = validated_data.get("items")
        for item in items:
            product_store = ProductStoreModel.objects.filter(store__id=item.get("stock"),
                                                             product__id=item.get("product")).first()
            SaleStoreOutProjectModel.objects.create(**{
                "sale_project": sale_order,
                "batch": batch,
                "product_store": product_store,
                "count": item.get("callin_count")
                })
        # except Exception, e:
        #     print e
        #     raise serializers.ValidationError(e.message)
