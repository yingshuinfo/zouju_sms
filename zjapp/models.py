# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser, Permission
from rest_framework import serializers

from zjapp.uitls import project_number


class ModuleModel(models.Model):
    name = models.CharField(help_text=u"模块名称", max_length=255, blank=True, null=True)


class DepartmentModel(models.Model):
    name = models.CharField(help_text=u"部门名称", max_length=255, null=True, blank=True)
    mark = models.TextField(help_text=u"备注", null=True, blank=True)


class CategoryModuleModel(models.Model):
    category = models.ForeignKey('CategoryModel', help_text=u'角色', related_name="modules")
    module = models.ForeignKey(ModuleModel, help_text=u'模块', related_name="categorys")
    is_view = models.BooleanField(help_text=u'是否查看', default=False)
    is_opt = models.BooleanField(help_text=u'是否操作', default=False)


class CategoryModel(models.Model):
    name = models.CharField(help_text=u"类别名称", max_length=255, null=True, blank=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(CategoryModel, self).save()
        for module in ModuleModel.objects.all():
            CategoryModuleModel.objects.get_or_create(category=self, module=module)


class User(AbstractUser):
    category = models.ForeignKey(CategoryModel, related_name="user", null=True, blank=True)
    department = models.ForeignKey(DepartmentModel, related_name="users", null=True, blank=True)
    name = models.CharField(help_text=u"姓名", max_length=255, null=True, blank=True)
    mark = models.TextField(help_text=u"备注", null=True, blank=True)
    page_permission = models.IntegerField(help_text=u"页面权限", default=0)


# Create your models here.
# 产品管理/                                      02-Apr-2018 11:19                   -
# 供应商/                                         02-Apr-2018 11:20                   -
# 客户管理模块/                                02-Apr-2018 11:20                   -
# 库存模块/                                      02-Apr-2018 11:21                   -
# 财务管理模块/                                02-Apr-2018 11:21                   -
# 销售管理模块/

class ProjectModel(models.Model):
    '''
    产品管理
        产品编码
        品牌名称
        产品名称
        图片
        卖方建议零售价
        采购单价
        数量
        编辑
    '''
    number = models.CharField(help_text=u"产品编码", max_length=255, default=project_number)
    brand_name = models.CharField(help_text=u"品牌名称", max_length=255, null=True, blank=True)
    name = models.CharField(help_text=u"产品名称", max_length=255, null=True, blank=True)
    _images = models.CharField(help_text=u"图片", max_length=255, default='images.jpg')
    mark = models.TextField(help_text=u"备注", null=True, blank=True)
    create_time = models.DateTimeField(help_text=u"创建时间", auto_now_add=True)

    @property
    def images(self):
        return "/".join(["http://47.93.213.40:8010/media", self._images])

    @images.setter
    def images(self, value):
        self._images = value

    class Meta:
        permissions = (
            ('view_project', 'Can View Project Model'),
        )


class SupplierModel(models.Model):
    '''
    供应商
        供应商名称、
        编码、
        供应商分类、
        联系人、
        联系电话、
        账号、
        税号、
        默认付款方式、
        账期、
        国家、
        发货地址、
        备注、
        合同查看、
        编辑
    '''
    name = models.CharField(help_text=u"供应商名称", max_length=255, null=True, blank=True)
    number = models.CharField(help_text=u"编码", max_length=255, null=True, blank=True)
    supplier_type = models.CharField(help_text=u"供应商分类", max_length=255, null=True, blank=True)
    lxr = models.CharField(help_text=u"联系人", max_length=255, null=True, blank=True)
    lxdh = models.CharField(help_text=u"联系电话", max_length=255, null=True, blank=True)
    zhanghao = models.CharField(help_text=u"账号", max_length=255, null=True, blank=True)
    shuihao = models.CharField(help_text=u"税号", max_length=255, null=True, blank=True)
    fkfs = models.CharField(help_text=u"付款方式", max_length=255, null=True, blank=True)
    zhangqi = models.CharField(help_text=u"账期", max_length=255, null=True, blank=True)
    country = models.CharField(help_text=u"国家", max_length=255, null=True, blank=True)
    address = models.CharField(help_text=u"发货地址", max_length=255, null=True, blank=True)
    mark = models.CharField(help_text=u"备注", max_length=255, null=True, blank=True)
    hetong = models.CharField(help_text=u"合同", max_length=255, null=True, blank=True)


class PurchaseOrderModel(models.Model):
    '''
    进货单
        订单号
        供货商类型、
        供货商/供货人员
        全额预付货物数
        全额预付日期
        全额预付金额
        预付比例
        预付金额
        预付日期
        货物数量
        尾款金额
        尾款日期
        总货值
        备注
        编辑
    '''
    number = models.CharField(help_text=u"订单号", max_length=255, default=project_number)
    Supplier_type = models.CharField(help_text=u"供应商分类", max_length=255, null=True, blank=True)
    supplier = models.ForeignKey(SupplierModel, help_text='供应商', related_name='saleorder_customer', null=True,
                                 blank=True)
    all_pre_pay_count = models.IntegerField(help_text=u"全额预付货物数", default=0)
    all_pre_pay_date = models.DateField(help_text=u"全额预付日期", null=True, blank=True)
    all_pre_pay_money = models.FloatField(help_text=u"全额预付金额", null=True, blank=True)
    pre_pay_ratio = models.FloatField(help_text=u"预付比例", default=0.00)
    pre_pay_date = models.DateField(help_text=u"预付日期", null=True, blank=True)
    pre_pay_money = models.FloatField(help_text=u"预付金额", null=True, blank=True)
    count = models.IntegerField(help_text=u"货物数量", default=0)
    balance_pay_money = models.FloatField(help_text=u"尾款金额", default=0.00)
    balance_pay_date = models.DateField(help_text=u"尾款日期", null=True, blank=True)
    all_mongey = models.FloatField(help_text=u"总货值", default=0.00)
    mark = models.TextField(help_text=u"备注", null=True, blank=True)
    create_time = models.DateTimeField(help_text=u"创建日期", auto_now_add=True)

    class Meta:
        permissions = (
            ('view_purchaseprdermodel', 'Can View PurchaseOrderModel'),
        )


class OrderProjectModel(models.Model):
    '''
    订单
    产品
    卖方建议零售价
    采购单价
    数量

    '''
    order = models.ForeignKey(PurchaseOrderModel, help_text="订单", related_name='purchase_order_project')
    project = models.ForeignKey(ProjectModel, help_text="产品", related_name='project_order')
    count = models.IntegerField(help_text=u"数量", default=0)
    suggest_price = models.FloatField(help_text=u"卖方建议零售价", default=0.00)
    purchase_price = models.FloatField(help_text=u"采购单价", default=0.00)

    class Meta:
        permissions = (
            ('view_orderprojectmodel', 'Can View order project model'),
        )


class CustomerModel(models.Model):
    '''
    客户管理模块
        客户编号、
        客户名称、
        姓名、
        拼音、
        证件号、
        送货地址、
        生日、
        客户类型、
        家庭成员姓名、
        小孩年龄、
        小孩数量、
        折扣核定、
        付款方式、
        接待业务员、
        开户日期、
        联系人、
        联系方式、
        Email、
        微信号、
        欠款限额、
        初期应收额、
        应收额、
        积分、
        已收额、
        累计消费金额、
        累计积分、
    '''
    number = models.CharField(help_text=u"客户编号", max_length=255, null=True, blank=True)
    kname = models.CharField(help_text=u"客户名称", max_length=255, null=True, blank=True)
    name = models.CharField(help_text=u"姓名", max_length=255, null=True, blank=True)
    name_py = models.CharField(help_text=u"拼音", max_length=255, null=True, blank=True)
    zjh = models.CharField(help_text=u"证件号", max_length=255, null=True, blank=True)
    shdz = models.CharField(help_text=u"送货地址", max_length=255, null=True, blank=True)
    sr = models.CharField(help_text=u"生日", max_length=255, null=True, blank=True)
    khlx = models.IntegerField(help_text=u"客户类型", null=True, blank=True)
    jtcyxm = models.CharField(help_text=u"家庭成员姓名", max_length=255, null=True, blank=True)
    xhsl = models.CharField(help_text=u"小孩年龄", max_length=255, null=True, blank=True)
    xhnl = models.CharField(help_text=u"小孩数量", max_length=255, null=True, blank=True)
    zkhd = models.CharField(help_text=u"折扣核定", max_length=255, null=True, blank=True)
    fkfs = models.CharField(help_text=u"付款方式", max_length=255, null=True, blank=True)
    jdywy = models.CharField(help_text=u"接待业务员", max_length=255, null=True, blank=True)
    khrq = models.DateField(help_text=u"开户日期", null=True, blank=True)
    lxr = models.CharField(help_text=u"联系人", max_length=255, null=True, blank=True)
    lxfs = models.CharField(help_text=u"联系方式", max_length=255, null=True, blank=True)
    email = models.CharField(help_text=u"Email", max_length=255, null=True, blank=True)
    wxh = models.CharField(help_text=u"微信号", max_length=255, null=True, blank=True)
    qkxe = models.CharField(help_text=u"欠款限额", max_length=255, null=True, blank=True)
    ljjf = models.CharField(help_text=u"累计消费金额", max_length=255, null=True, blank=True)
    ljxfje = models.CharField(help_text=u"累计积分", max_length=255, null=True, blank=True)
    create_time = models.DateTimeField(verbose_name=u'录入时间', auto_now_add=True, blank=True)


class StockrModel(models.Model):
    '''
    库存模块
        仓库名称
        仓库编码
        仓库分类
        联系人
        联系电话
        是否在用
        仓库位置
    '''
    name = models.CharField(help_text=u"仓库名称", max_length=255, blank=True, null=True)
    number = models.CharField(help_text=u"仓库编码", max_length=255, blank=True, null=True)
    category = models.CharField(help_text=u"仓库分类", max_length=255, blank=True, null=True)
    linkman = models.CharField(help_text=u"联系人", max_length=255, blank=True, null=True)
    tel = models.CharField(help_text=u"联系电话", max_length=255, blank=True, null=True)
    is_using = models.BooleanField(help_text=u"是否在用", default=True)
    place = models.CharField(help_text=u"仓库位置", max_length=255, blank=True, null=True)


class OrderProductStoreModel(models.Model):
    order_project = models.ForeignKey(OrderProjectModel, help_text=u"订单货物", related_name='store')
    batch = models.IntegerField(help_text=u"批次")
    count = models.IntegerField(help_text=u'数量')
    store = models.ForeignKey(StockrModel, help_text=u'仓库', related_name='order_product')
    state = models.IntegerField(help_text=u'清关状态', default=0)
    create_time = models.DateTimeField(verbose_name=u'录入时间', auto_now_add=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(OrderProductStoreModel, self).save()
        product_store, _ = ProductStoreModel.objects.get_or_create(product=self.order_project.project, store=self.store)
        product_store.count += self.count
        product_store.save()

    # def delete(self, using=None, keep_parents=False):
    #     super(OrderProductStoreModel, self).delete()
    #     product_store, _ = ProductStoreModel.objects.get_or_create(product=self.order_project.project, store=self.store)
    #     product_store.count -= self.count
    #     if product_store.count < 0:
    #         product_store.count = 0
    #     product_store.save()


class ProductStoreModel(models.Model):
    store = models.ForeignKey(StockrModel, help_text=u"仓库", related_name='products')
    product = models.ForeignKey(ProjectModel, help_text=u'产品', related_name='store')
    count = models.IntegerField(help_text=u'数量', default=0)

    def change_count(self, count):
        self.count += count
        if self.count < 0:
            raise serializers.ValidationError(u'调库失败，库存不足！')
        self.save()


# 销货管理
class SaleOrderModel(models.Model):
    '''
    销售订单号、
    产品详情、
    销售日期、
    预付比例、
    预付金额、
    预付日期、
    尾款金额、
    尾款日期、
    总销售额、
    销售类型、
    经销商/销售人员
    '''

    number = models.CharField(help_text=u"序列号", max_length=255)
    all_pre_pay_count = models.IntegerField(help_text=u"全额预付货物数", default=0)
    all_pre_pay_date = models.DateField(help_text=u"全额预付日期", null=True, blank=True)
    all_pre_pay_money = models.FloatField(help_text=u"全额预付金额", null=True, blank=True)

    pre_pay_ratio = models.FloatField(help_text=u"预付比例", default=0.00)
    pre_pay_date = models.DateField(help_text=u"预付日期", null=True, blank=True)
    pre_pay_money = models.FloatField(help_text=u"预付金额", null=True, blank=True)

    balance_pay_money = models.FloatField(help_text=u"尾款金额", default=0.00)
    balance_pay_date = models.DateField(help_text=u"尾款日期", null=True, blank=True)

    count = models.IntegerField(help_text=u"货物数量", default=0)
    all_mongey = models.FloatField(help_text=u"总货值", default=0.00)
    sale_type = models.CharField(help_text=u"销售类型", max_length=50)
    customer = models.ForeignKey(CustomerModel, help_text='客户', related_name='saleorder_customer', null=True,
                                 blank=True)
    mark = models.TextField(help_text=u"备注", null=True, blank=True)
    create_time = models.DateTimeField(help_text=u"生成日期", auto_now_add=True)


class SaleOrderProjectModel(models.Model):
    '''
    销货单货物
    '''
    order = models.ForeignKey(SaleOrderModel, help_text=u"订单", related_name=u"sale_order_project")
    project = models.ForeignKey(ProjectModel, help_text=u"货物", related_name='project_saleorder')
    count = models.IntegerField(help_text=u"数量", default=0)
    suggest_price = models.FloatField(help_text=u"建议零售价", default=0.00)
    sale_price = models.FloatField(help_text=u"出货单价", default=0.00)


class SaleStoreOutProjectModel(models.Model):
    '''
    销货单出货
    '''
    sale_project = models.ForeignKey(SaleOrderModel, help_text=u"销货单", related_name='store_sale')
    batch = models.IntegerField(help_text=u"批次", default=1)
    count = models.IntegerField(help_text=u'数量')
    product_store = models.ForeignKey(ProductStoreModel, help_text=u'产品仓库', related_name='sale_order_product')
    create_time = models.DateTimeField(verbose_name=u'录入时间', auto_now_add=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id:
            # 仓库还原
            pre_self = SaleStoreOutProjectModel.objects.get(id=self.id)
            pre_self.product_store.count += pre_self.count
            pre_self.product_store.save()

        if self.product_store.count < self.count:
            raise serializers.ValidationError(u"该仓库库存不足")
        else:
            self.product_store.count -= self.count
            self.product_store.save()
            super(SaleStoreOutProjectModel, self).save()

    def delete(self, using=None, keep_parents=False):
        self.product_store.count += self.count
        self.product_store.save()
        super(SaleStoreOutProjectModel, self).delete()


class FinanceModel(models.Model):
    '''
    财务管理模块
        财务编号、类别、记录日期、收付款对象、单据号、
        金额、优惠额、付款方式、收付款人、是否作废、
        作废人员、作废时间、详情查看、编辑
    '''
    number = models.CharField(help_text=u"财务编号", max_length=255, blank=True, null=True)
    category = models.IntegerField(help_text=u"类别", blank=True, null=True)
    jlrq = models.DateField(help_text=u"记录日期", blank=True, null=True)
    sfkdx = models.CharField(help_text=u"收付款对象", max_length=255, blank=True, null=True)
    djh = models.CharField(help_text=u"单据号", max_length=255, null=True, blank=True)
    je = models.FloatField(help_text=u"金额", blank=True, null=True)
    yhe = models.FloatField(help_text=u"优惠额", blank=True, null=True)
    fkfs = models.CharField(help_text=u"付款方式", max_length=255, blank=True, null=True)
    sfkr = models.CharField(help_text=u"收付款人", max_length=255, blank=True, null=True)
    sfzf = models.BooleanField(help_text=u"是否作废", default=True)
    zfry = models.CharField(help_text=u"作废人员", max_length=255, blank=True, null=True)
    zfsj = models.DateField(help_text=u"作废时间", blank=True, null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id:
            pre_self = FinanceModel.objects.get(id=self.id)
            print(pre_self.djh)
            print(self.djh)


class MarketModel(models.Model):
    '''
    销售管理模块
    '''


class FileModel(models.Model):
    file = models.FileField(help_text=u"文件", upload_to="project/")
    create_time = models.DateTimeField(auto_now_add=True)
