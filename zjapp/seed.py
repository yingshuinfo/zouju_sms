# coding:utf-8
import sys
import os
import django

sys.path.extend(['D:\\PycharmProjects\\zouju_sms'])
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "zouju_sms.settings")
django.setup()
from faker import Faker

fake = Faker('zh_CN')

from zjapp.models import PurchaseOrderModel, OrderProjectModel, ProjectModel, SaleOrderModel, SaleOrderProjectModel, \
    ModuleModel


def add_projects():
    ModuleModel.objects.create(name=u'系统设置')
    ModuleModel.objects.create(name=u'产品管理')
    ModuleModel.objects.create(name=u'供应商')
    ModuleModel.objects.create(name=u'客户管理')
    ModuleModel.objects.create(name=u'库存管理')
    ModuleModel.objects.create(name=u'进货管理')
    ModuleModel.objects.create(name=u'销货管理')
    ModuleModel.objects.create(name=u'财务管理')



if __name__ == '__main__':
    add_projects()
